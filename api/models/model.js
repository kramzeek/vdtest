'use strict';

var mongoose = require('mongoose');
var Schema   = mongoose.Schema;


var ObjectSchema = new Schema({
  key: {
    type: String,
    required: 'Please enter a key name'
  },
  value: {
    type: Object,
    required: 'Please enter a value'
  },
  timestamp: {
    type: Date,
    default: Date.now
  }
});

module.exports = mongoose.model('VDObjects', ObjectSchema);
