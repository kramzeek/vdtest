'use strict';

var mongoose = require('mongoose'),
  ObjectModel = mongoose.model('VDObjects');

exports.create = function(req, res) {
  
  var new_object = new ObjectModel(req.body);
  
  new_object.save('-_id key value timestamp', function(err, vd_object) {
    if (err){
      res.send(err);
    }
    var vd_objectObj = vd_object.toObject();
    delete vd_objectObj.__v;
    delete vd_objectObj._id;
    res.json(vd_objectObj);
  });
};

exports.show_value = function(req, res) {
  if (typeof req.query.timestamp == 'undefined'){
    ObjectModel.findOne({'key':req.params.key}, '-_id value', { sort: {'timestamp' : -1} }, function(err, vd_object) {
      if (err){
        res.send(err);
      }
      res.json(vd_object);
    });
  } else {
    var ts = new Date(req.query.timestamp*1000);
    ObjectModel.findOne({'key':req.params.key, 'timestamp' : {'$lte' : ts}}, '-_id value', { sort: {'timestamp' : -1} }, function(err, vd_object) {
      if (err){
        res.send(err);
      }
      res.json(vd_object);
    });
  }
};  
