'use strict';
  module.exports = function(app) {
  var vd_object = require('../controllers/objectController');

  app.route('/object')
    .post(vd_object.create);


  app.route('/object/:key')
    .get(vd_object.show_value);
};
